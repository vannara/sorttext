﻿
using System.Collections.Generic;
using TextAnalyzerLib;

namespace ConsoleAppTestTextAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Enter the text to analyze :");
            string input = System.Console.ReadLine();
            Dictionary<string, int> result = TextAnalyzer.GetSortedWordAndCount(input);
            foreach (var word in result)
            {
               System.Console.WriteLine(word.Value + " " + word.Key +"\t");
            }
            
        }
    }
}
