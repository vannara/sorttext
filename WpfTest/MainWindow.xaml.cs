﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using TextAnalyzerLib;
namespace WpfTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnAnalyze_Click(object sender, RoutedEventArgs e)
        {
            TextRange textRange = new TextRange(rtxtInput.Document.ContentStart, rtxtInput.Document.ContentEnd);
            Dictionary<string, int> result = TextAnalyzer.GetSortedWordAndCount(textRange.Text);
            lvResult.ItemsSource = result;
        }
    }
}
