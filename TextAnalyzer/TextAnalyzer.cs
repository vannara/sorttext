﻿using System;
using System.Collections.Generic;

namespace TextAnalyzerLib
{
    public static class TextAnalyzer
    {
        /// <summary>
        /// Analyze the content of the text by sorting (ascending) the list of word by its length then by ASCII.
        /// The Dictionary key will the word and Dictionary value will be the number of its appearance in the text
        /// </summary>
        /// <param name="text">the content to be analyzed</param>
        /// <returns>word as Dictionary value and its count as Dictionary value</returns>
        public static Dictionary<string, int> GetSortedWordAndCount(string text)
        {
            Dictionary<string, int> countedWordsDic = new Dictionary<string, int>();            
            List<string> list = SortTextByLengthAndByAlphabet(text);
            foreach(string s in list)
            {
                if (!countedWordsDic.ContainsKey(s))
                {
                    countedWordsDic.Add(s, 1);
                } else
                {
                    // Add the count
                    countedWordsDic[s] = countedWordsDic[s] + 1;

                }
            }
            return countedWordsDic;
        }
        
        /// <summary>
        /// sort text by its length then by alphabet
        /// </summary>
        /// <param name="text"></param>
        /// <returns>List of word after sorted</returns>
        public static List<string> SortTextByLengthAndByAlphabet(string text)
        {
            string temp = string.Empty;
            List<string> words = GetListOfWord(text);
            words = SortTextByLength(words);
            for (int i = 0; i < words.Count; i++)
            {
                for (int j = i + 1; j < words.Count; j++)
                {
                    if (words[i].Length == words[j].Length && words[i][0] > words[j][0])
                    {                      
                        temp = words[j];
                        words[j] = words[i];
                        words[i] = temp;                        
                    }
                }
            }

            return words;
        }

        /// <summary>
        /// split a text content to array of string
        /// </summary>
        /// <param name="content"></param>
        /// <returns>list of words</returns>
        private static List<string> GetListOfWord(string content)
        {
            List<string> words = new List<string>();
            string[] contents = content.Split(new char[] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach(string s in contents)
            {                
                if(ContainIgnoreCharactors(s))
                {
                    // Hope user writes English grammatically correct i.e word. word! word? word,
                    string newString = s.Substring(0, s.Length -1);
                    words.Add(newString);
                } else
                {
                    words.Add(s);
                }
            }
            return words;
        }
       
        /// <summary>
        /// sort text by its length ascendingly
        /// </summary>
        /// <param name="words"></param>
        /// <returns>list of words after sorted</returns>
        private static List<string> SortTextByLength(List<string> words)
        {
            string temp = string.Empty;
            for (int i = 0; i < words.Count; i++)
            {
                for (int j = i + 1; j < words.Count; j++)
                {
                    if (words[i].Length > words[j].Length)
                    {
                        temp = words[j];
                        words[j] = words[i];
                        words[i] = temp;
                    }
                    
                }
            }
        return words;
        }
        
        /// <summary>
        /// check if the content of the string contains ':' ';' '.' ',' '?' or '!'
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static bool ContainIgnoreCharactors(string text)
        {
            return text.Contains(":") 
                || text.Contains(";") 
                || text.Contains(".") 
                || text.Contains("!") 
                || text.Contains("?") 
                || text.Contains(",");
        }

    }
}
